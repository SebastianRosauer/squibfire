function playLoop(connection){
  let dispatcher = connection.play(config.audioFileLocation);
  dispatcher.on('error', (error)=>{
    logger.error(`An error during playback occured: ${error}`);
  });
  dispatcher.on('finish', end => {
    dispatcher = playLoop(connection);
  })
  return connection;
}

function playOnce(connection){
  const dispatcher = connection.play('src/media/important.mp3');
  dispatcher.on('finish', () => {
    playLoop(connection);
  });
}

exports.playLoop = playLoop;
exports.playOnce = playOnce;