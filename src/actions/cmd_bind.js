const fs = require('fs');
const { loggers } = require('winston');

function bind(message) {
  if (!message.member.voice.channelID){
    message.channel.send(`Please join a voice channel before using ${config.cmdPrefix}bind`);
    return;
  }

  let guildBinding = bindings.filter((binding) => {
    return binding.guildID === message.guild.id;
  })[0];

  if (guildBinding) {
    guildBinding.channel = message.member.voice.channelID
  } else {
    bindings.push({
      guildID: message.guild.id,
      channel: message.member.voice.channelID
    });
  }
  
  fs.writeFileSync('bindings.json', JSON.stringify(bindings));
  message.channel.send(`Bound to channel ${message.member.voice.channel.name}`);
}

module.exports = bind;