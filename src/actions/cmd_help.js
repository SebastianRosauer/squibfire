function help(message){
  let audioFileName = config.audioFileLocation.split('/');
  audioFileName = audioFileName[audioFileName.length -1];
  message.channel.send(
  `Hi, I'm Squibfire, your trusty ambience sound player!
  
  Just join a voice channel and type ${config.cmdPrefix}bind to tell me where to wait for some folks!
  My ambience sound is currently set to ${audioFileName}.
  
  🦆`);
}

module.exports = help;