const { playOnce } = require('./audioManager');

let nextPraise = 0;

function praise(message){

  if (message.guild.voice.connection){
    if (Date.now() > nextPraise) {
      playOnce(message.guild.voice.connection);
      nextPraise = Date.now() + 60000;
    } else {
      message.channel.send('The almighty duck shall only be praised once a minute.');
    }
  } else {
    message.channel.send('Need some audience to praise ...');
  }
}

module.exports = praise;