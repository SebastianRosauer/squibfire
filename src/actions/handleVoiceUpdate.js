const {playLoop} = require('./audioManager');

function handleVoiceUpdate(oldState, newState){
  logger.debug('caught voiceUpdate');

  let binding = bindings.filter((el)=>{
    return (el.guildID === newState.guild.id) ? true : false;
  });
  if (binding === []){
    return;
  } else {
    binding = binding[0];
  }

  // Did the user join (not leave) a channel? Is this the channel the instance is bound to? Is the instance not yet connected?
  if (newState.channel && binding.channel === newState.channel.id && !newState.guild.me.voice.channel) {
    newState.channel.join().then(connection =>{
        playLoop(connection);        
    }).catch(error => {
      logger.error(`Error in channel ${newState.channel.name} on ${newState.guild.name} (${binding}): ${error}`);
    });

  } else if (oldState.channel && binding.channel === oldState.channel.id && oldState.guild.me.voice.channel) {
    const connectedMembersCount = (oldState.guild.voiceStates.cache.filter((voiceState)=>{
      return voiceState.channelID === binding.channel;
    }).array().length);
    if (connectedMembersCount <= 1){
      oldState.channel.leave();
    }
  }
}

module.exports = handleVoiceUpdate;