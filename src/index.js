const cmd_bind = require('./actions/cmd_bind');
const cmd_praise = require('./actions/cmd_praise');
const cmd_help = require('./actions/cmd_help');
const handleVoiceUpdate = require('./actions/handleVoiceUpdate');
const fs = require('fs');
const discord = require('discord.js');
const client = new discord.Client();
const winston = require('winston');
global.logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      filename: 'log',
      level: 'warn'
    }),
    new winston.transports.Console({
      level: 'info'
    })
  ]
});

// Setup
if (!fs.existsSync('config.json')){
  logger.info('Creating new config.json ...');
  global.config = {
    cmdPrefix: '!',
    token: '',
    audioFileLocation: ''
  }
  fs.writeFileSync('config.json', JSON.stringify(config));
} else {
  global.config = JSON.parse(fs.readFileSync('config.json'));
}

if (!config.token || config.token == ''){
  logger.error('Please specify a client token in config.json');
  process.exit();
}
if (!config.audioFileLocation ||  config.audioFileLocation == ''){
  logger.error('Please specify an audiofile in config.json');
  process.exit();
}
client.login(config.token).then(()=>{
  logger.info('Successfully logged in');
}).catch((err)=>{
  logger.error('Login failed. Reason: ' +err);
  process.exit();
});

if (fs.existsSync('bindings.json')){
  global.bindings = JSON.parse(fs.readFileSync('bindings.json'));
} else {
  bindings = [];
}

// Event Listeners
client.once('ready', ()=>{
  console.log('Ready!');
});

client.on('message', (message) => {
  if (message.content.startsWith(`${config.cmdPrefix}bind`)){
    logger.debug('caught bind');
    cmd_bind(message);
  } else if (message.content.startsWith(`${config.cmdPrefix}praise`)){
    logger.debug('caught praise');
    cmd_praise(message);
  } else if (message.content.startsWith(`${config.cmdPrefix}help`)){
    logger.debug('caught help');
    cmd_help(message);
  }
});

client.on('voiceStateUpdate', handleVoiceUpdate);

